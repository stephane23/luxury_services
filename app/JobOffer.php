<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobOffer extends Model 
{

    protected $table = 'job_offers';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('reference', 'society_name', 'contact_name', 'contact_email', 'contact_phone', 'location', 'description', 'activated', 'notes', 'job_title', 'job_type', 'job_category', 'closing_at', 'salary', 'client_id');

    public function user()
    {
        return $this->belongsToMany('App\JobApplication');
    }

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

}