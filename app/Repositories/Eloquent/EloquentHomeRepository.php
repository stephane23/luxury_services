<?php

namespace App\Repositories\Eloquent;

use App\Home;
use App\Repositories\Contracts\HomeRepository;

use Kurt\Repoist\Repositories\Eloquent\AbstractRepository;

class EloquentHomeRepository extends AbstractRepository implements HomeRepository
{
    public function entity()
    {
        return Home::class;
    }
}
