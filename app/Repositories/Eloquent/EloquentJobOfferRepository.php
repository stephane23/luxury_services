<?php namespace App\Repositories\Eloquent;

use App\JobOffer;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Contracts\JobOfferRepository;
use Kurt\Repoist\Repositories\Eloquent\AbstractRepository;


class EloquentJobOfferRepository extends AbstractRepository implements JobOfferRepository
{
    public function entity()
    {
        return JobOffer::class;
    }

    public function __construct(JobOffer $model)
   {
       parent::__construct($model);
       $this->model = $model;
   }

    public function showOffer($id)
    {   
        return $this->model->where('id', $id)->first();
    }
}
