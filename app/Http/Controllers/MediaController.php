<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class MediaController extends Controller
{
    public function show($partialPath)
    {
        $fullPath = storage_path('app/images' . $partialPath);

        if (!File::exists($fullPath)) {
            abort(404);
        }
    
        $file = File::get($fullPath);
        $type = File::mimeType($fullPath);
    
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
    
        return $response;
    }
}
