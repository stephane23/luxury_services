<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JobOffer;
use Auth;
use Mail;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('guest');
        $jobsList = JobOffer::orderBy("created_at", "desc")->take(4)->get();
    
        return view('homepage', compact('jobsList', 'totalRounded'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        return view('contact');
    }

    public function sendMail(Request $request)
    {
        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $message = $request->input('message');

        Mail::send('mail', $data = ['firstname' => $firstname, 'lastname' => $lastname, 'email' => $email, 'phone' => $phone, 'message' => $message], function ($m) {
            $m->to('stef.peche.fernandes@gmail.com', 'stéphane fernandes');
        });
        
        return redirect()->back()->with('flash_message', 'Thank you, message sent');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function company()
    {
        return view('company');
    }
}
