<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller 
{

    public function __construct() 
    {
      $this->middleware('auth');
    }


  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show()
  {
    $user = auth()->user();

    if ( ! $user) {
      return 0;
    }
    $columns    = preg_grep('/(.+ed_at)|(.*id)/', array_keys($user->toArray()), PREG_GREP_INVERT);
    $per_column = 100 / count($columns);
    $total      = 0;
    foreach ($user->toArray() as $key => $value) {
      if ($value !== NULL && $value !== [] && in_array($key, $columns)) {
        $total += $per_column;
        $totalRounded = floor($total);
      }
    }
    return view('profile', compact('user', 'total', 'totalRounded'));  
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $requestData = $request->all();
    
    $profileData = array(
      'gender' => $requestData['gender'], 
      'first_name' => $requestData['first_name'], 
      'last_name' => $requestData['last_name'], 
      'address' => $requestData['address'], 
      'country' => $requestData['country'], 
      'nationality' => $requestData['nationality'],  
      'current_location' => $requestData['current_location'], 
      'birth_date' => $requestData['birth_date'], 
      'birth_place' => $requestData['birth_place'], 
      // 'email' => $requestData['email'], 
      // 'password' => $requestData['password'],
      'availability' => isset($requestData['availability'])?1:0, 
      'job_sector' => $requestData['job_sector'], 
      'experience' => $requestData['experience'], 
      'short_description' => $requestData['short_description'],
      'notes' => $requestData['notes'] ?? "",
    );

    // Si le CV est valide
    if ($request->hasFile('curriculum_vitae') && $request->file('curriculum_vitae')->isValid()) {
      // Je stocke le CV et je mémorise le path
      $file = $request->file('curriculum_vitae');
      $profileData["curriculum_vitae"] = str_replace('images/', '', $request->file("curriculum_vitae")->store("images"));
    }

    // Si le passport est valide
    if ($request->hasFile('passport') && $request->file('passport')->isValid()) {
      // Je stocke le fichier et je mémorise le path
      $file = $request->file('passport');
      $profileData["passport"] = str_replace('images/', '', $request->file('passport')->store('images'));
    }

    // Si le profil_picture est valide
    if ($request->hasFile('profile_picture') && $request->file('profile_picture')->isValid()) {
      // Je stocke le fichier et je mémorise le path
      $file = $request->file('profile_picture');
      $profileData["profile_picture"] = str_replace('images/', '', $request->file('profile_picture')->store('images'));
    }

    auth()->user()->update($profileData);
    
    return redirect()->back()->with("flash_message", "Profile has been completed!");
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy()
  {
    $user = auth()->user();
    $user->delete();

    auth()->logout();

    return view('homepage');
  }

  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array  $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
      return Validator::make($data, [
          'email' => 'required|string|email|max:255|unique:users',
          'password' => 'required|string|min:6|confirmed|same:password',
          'password_repeat' => 'required|same:password'
      ]);
  }
}

?>
