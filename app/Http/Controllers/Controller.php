<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Repositories\Eloquent\EloquentJobOfferRepository;
use App\Repositories\Eloquent\EloquentJobApplicationRepository;
use View;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /** @var EloquentJobOfferRepository */
    protected $jobOfferRepository;
    /** @var EloquentJobApplicationRepository */
    protected $jobApplicationRepository;

    public function __construct(EloquentJobOfferRepository $jobOfferRepository, EloquentJobApplicationRepository $jobApplicationRepository)
    {
        $this->jobOfferRepository = $jobOfferRepository;
        $this->jobApplicationRepository = $jobApplicationRepository;
        //$this->middleware('guest', ['except' => ['homepage', 'joboffer', 'contact', 'company']]);
        $this->middleware(function ($request, $next) {
            $user = auth()->user();
            if ( ! $user) {
            return $next($request);
            }
            $columns    = preg_grep('/(.+ed_at)|(.*id)/', array_keys($user->toArray()), PREG_GREP_INVERT);
            $per_column = 100 / count($columns);
            $total      = 0;
            foreach ($user->toArray() as $key => $value) {
                if ($value !== NULL && $value !== [] && in_array($key, $columns)) {
                $total += $per_column;
                $totalRounded = floor($total);
                }
            }
        View::share(compact('user', 'total', 'totalRounded'));
        return $next($request);
        });
    }
}
