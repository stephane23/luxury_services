<?php namespace App\Http\Controllers;

use App\JobApplication;
use Illuminate\Http\Request;
use App\Repositories\Eloquent\EloquentJobApplicationRepository;

class JobApplicationController extends Controller 
{
  protected $jobApplicationRepository;

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store($jobId)
  {
    $jobApplicationRepository = $this->jobApplicationRepository;
    $hasJobApplication = $jobApplicationRepository->hasJobApplication($jobId);
    
    if(!$hasJobApplication) {
      $this->jobApplicationRepository->create(array(
        'users_id' => auth()->id(), 
        'job_offers_id' => $jobId,    
      ));
    
    return back()->withConfirmation('You applied for this job.');
    } else {
      
      return back()->withError('You already applied for this job.');
      } 
  } 
}
?>
