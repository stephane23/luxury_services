<?php  namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;

class ClientController extends Controller 
{
  protected $client;

  public function __construct(Client $client) {
    $this->client= $client;
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $requestData = $request->all();

    $this->client->create(array(
      'society_name' => $requestData['society_name'], 
      'society_type' => $requestData['society_type'], 
      'contact_name' => $requestData['contact_name'], 
      'contact_job' => $requestData['contact_job'], 
      'contact_phone' => $requestData['contact_phone'], 
      'contact_email' => $requestData['contact_email'], 
      'notes' => $requestData['notes']));

    return back();
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $client = Client::find($id);
    $client->delete();
  }
  
}

?>