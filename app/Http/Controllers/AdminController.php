<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\JobOffer;
use App\JobApplication;
use App\Repositories\Eloquent\EloquentJobOfferRepository;

class AdminController extends Controller
{
  
  public function show()
  {
      $clientList = Client::all();
      return view('admin', compact('clientList'));
  }

  public function indexJobOffers()
  {
    $jobsList = JobOffer::orderBy("created_at", "desc")->get();
    return view('admin.index_job_offers', compact('jobsList', 'totalRounded'));
  }

  public function showJobOffer($id)
  {
    $jobOffer = $this->jobOfferRepository->showOffer($id);
    // get the current job offer
   $jobOfferId = JobOffer::find($id);
   // get previous job offer id
   $previous = JobOffer::where('id', '<', $jobOffer->id)->max('id');
   // get next job offer id
   $next = JobOffer::where('id', '>', $jobOffer->id)->min('id');
   $jobApplications = JobApplication::with('user')->where('job_offers_id', $id)->get(); 
   
    return view('admin.show_job_offer', compact('jobOffer', 'previous', 'next', 'totalRounded', 'jobApplications'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroyJobOffer($id)
  {
    $item = JobOffer::find($id);
    $item->delete();

    return redirect()->route('admin.index_job_offers');
  }

  public function showClients()
  {
      $clientList = Client::all();
      return view('admin.clients', compact('clientList'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroyClient($id)
  {
    $itemClient = Client::find($id);
    $itemClient->delete();

    return back();
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroyJobApplication($id)
  {
    $itemJobApplication = JobApplication::find($id);
    $itemJobApplication->delete();

    return back();
  }
  
  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function showProfile()
  {
    $user = auth()->user();


    if ( ! $user) {
      return 0;
    }
    $columns    = preg_grep('/(.+ed_at)|(.*id)/', array_keys($user->toArray()), PREG_GREP_INVERT);
    $per_column = 100 / count($columns);
    $total      = 0;
    foreach ($user->toArray() as $key => $value) {
      if ($value !== NULL && $value !== [] && in_array($key, $columns)) {
        $total += $per_column;
        $totalRounded = floor($total);
      }
    }

    return view('admin.profile', compact('user', 'total', 'totalRounded', 'userJobApplications'));  
  }  
}
