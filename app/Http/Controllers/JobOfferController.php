<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JobOffer;
use App\JobApplication;
use App\Repositories\Eloquent\EloquentJobOfferRepository;

class JobOfferController extends Controller 
{
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {

    $jobsList = JobOffer::orderBy("created_at", "desc")->get();
    return view('jobs.index', compact('jobsList', 'totalRounded'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $requestData = $request->all();

    $this->jobOfferRepository->create(array(
      'society_name' => $requestData['society_name'], 
      'contact_name' => $requestData['contact_name'], 
      'contact_email' => $requestData['contact_email'], 
      'contact_phone' => $requestData['contact_phone'], 
      'description' => $requestData['description'], 
      'activated' => $requestData['activated'],
      'notes' => $requestData['notes'],
      'job_title' => $requestData['job_title'],
      'job_type' => $requestData['job_type'],
      'location' => $requestData['location'],
      'job_category' => $requestData['job_category'],
      'salary' => $requestData['salary'],
      'closing_at' => $requestData['closing_at'],
      'client_id' => $requestData['client_id'],    
    ));
    return back();
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $jobOffer = $this->jobOfferRepository->showOffer($id);
   // get the current job offer
   $jobOfferId = JobOffer::find($id);
   // get previous job offer id
   $previous = JobOffer::where('id', '<', $jobOffer->id)->max('id');
   // get next job offer id
   $next = JobOffer::where('id', '>', $jobOffer->id)->min('id');
    return view('jobs.show', compact('jobOffer', 'previous', 'next', 'totalRounded'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  
  
}

?>