<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobApplication extends Model 
{

    protected $table = 'job_applications';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('users_id', 'job_offers_id');

    public function user()
    {
        return $this->belongsTo('App\User', 'users_id', 'id');
    }

    public function jobOffer()
    {
        return $this->hasOne('App\JobOffer');
    }

}