<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
        <title>Luxury services</title>
        @include('partials/head')
         <!--  favicon -->
    <link rel="shortcut icon" href="assets/img/ico/fav.png">

    <script>
        window.isMobile = (function() {
            var check = false;
            (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
            return check;
        })();
    </script>
    <script>
        function onAppReady(handler) {
            $(handler);
        }
    </script>
    <link href="../../assets/css/main.css" rel="stylesheet">

    <link href="http://kys.idmkr.io/cache/assets/frontend.kys.materialize.a1042cadda540464eafd9350bb40433a_1513864502.css" rel="stylesheet">
    <link href="http://kys.idmkr.io/cache/assets/frontend.kys.bootstrap.457005dcd8ab887d5f47791eaa1e3a26_1513863974.css" rel="stylesheet">
    <link href="http://kys.idmkr.io/cache/assets/frontend.kys.style.117d2669c8fdd6628dab9a45764bc512_1521725861.css" rel="stylesheet">
    <link href="http://kys.idmkr.io/cache/assets/frontend.kys.custom.6b4d49f73674949c14f1216c6eddcb85_1524615683.css" rel="stylesheet">
    <link href="http://kys.idmkr.io/cache/assets/frontend.kys.profile.33ab50bec585701ed711060403605d20_1522334401.css" rel="stylesheet">

    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.modernizr.1c9d48a1e28b608c156f4de214d48a4f_1513864496.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery.5b69aaaa5f04c52e66580a2dc32d1d69_1513864495.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.animated-headline.88cb936096e0f8ab70515bc801d8342e_1513864493.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.bootstrap-tabcollapse.34017c51086f0e87aa46196565972d00_1513864493.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.coundown-timer.c8cf6063f2de9fb102572c2c5cbb35d9_1518615831.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.equalheight.317d8490808d596c8a0f1758d495856c_1513864493.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.menuzord.1b3c610e36a938dda7612403eff6e3cf_1513864496.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.smooth-menu.561efb1e63c25f6459bb7d6b910fd0e0_1513864497.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.imagesloaded.cd624dbd9f9cedc0517e2a007e5a9e99_1513864494.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.owl-carousel.9a2fb1bcafcaeab824262566db2bddef_1513864521.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.wow.8dcaf0f70a4ccabe591dd6faf3c07bdc_1513864497.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.scripts.b4ebfafba9aad4e481cd55b4053b8783_1523435133.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.bootstrap.457bf879eca11f66e2de154003ed0f50_1513863978.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.materialize.bfaa8e3bbc89c450471bc8cc1b9a0959_1513864503.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery-easing.502fedff28c30ca28b69b469d4f49248_1513864495.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery-countTo.2216a6bac856d869218448a54e6bbc8c_1513864495.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery-sticky.980853b199a69acce57cf93d5a9a3767_1513864496.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery-stellar.55e412525b98bd15386ee154989c3e48_1513864495.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery-inview.0fee8e871d8e8332b973955f71c14a0b_1513864495.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery-shuffle.59f156c2072439edc7423e30643fed01_1520436199.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery-magnific-popup.5830d3754f9847bc3affd25657b7464e_1513864498.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.jquery-flexslider.6fd2216059a696cd152f2e674c4e2866_1513863989.js"></script>
    <script src="http://kys.idmkr.io/cache/assets/frontend.kys.platform-validate.740802da806d6a8534f41a5e804c739f_1513853856.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    {{-- <style>
        select {
            display: block!important;
        }
    </style> --}}
</head>
<body>
@include('partials/header-admin')
    <section class="gray-bg">
        <div class="container">                
        </div>
    </section>
    <section class="gray-bg">
            <div class="container">
                <div class="row">
                    <div class="score-container">
                        <br><br>
                        <h3>Admin Space</h3>
                    </div>
                </div>
            </div>
            <br><hr>
    </section>
    <section class="gray-bg">
        <div class="container-fluid">
            <div class="row">
                <div class="col s2"  style="border-right: 1px solid #5b5b5b; box-shadow: 2px 2px 6px black">
                    <ul>
                        <li><a href="#!">option 1</a></li>
                        <li><a href="#!">option 2</a></li>
                        <li><a href="#!">option 3</a></li>
                        <li><a href="#!">option 4</a></li>
                        <li><a href="#!">option 5</a></li>
                        <li><a href="#!">option 6</a></li>
                        <li><a href="#!">option 7</a></li>
                        <li><a href="#!">option 8</a></li>
                    </ul>
                </div>
                <div class="col s10" style="height 20px">
                    <canvas id="myChart" height="20vh" style="border: 1px solid black; width: 100%"></canvas>
                </div>
            </div>
        </div>
        <br><br>
    </section>
    <section>
        <div class="row">
            <div class="col s12">
                <ul class="tabs">
                    <li class="tab col s6"><a class="active"  href="#createJobs">Creation of a job offer</a></li>
                    <li class="tab col s6"><a href="#createClient">Creation of a client</a></li>
                    <li class="tab col s6"><a href="#createUser">Creation of a candidate</a></li>
                </ul>
            </div>
            <!--Job Offer-->
            <div id="createJobs" class="col s12">
                <form method="POST" action="{{ action('JobOfferController@store') }}" accept-charset="UTF-8" id="candidateForm" role="form" data-parsley-validate="" enctype="multipart/form-data">
                @csrf
                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                    <section class="section-padding">
                        <div class="container">
                            <div class="row">
                                <h3 class="text-extrabold">Create a job offer</h3>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <label for="society_name">Society Name</label>
                                        <br>
                                        @foreach($clientList as $client)
                                        <input type="hidden" name="client_id" value="{{ $client['id'] }}">
                                        @endforeach
                                            <select name="society_name" id="society_Name" required>
                                                @forelse ($clientList as $client)                                                  
                                                    <option>{{ $client['society_name']}}</option>
                                                @empty
                                                    <option>create a client</option>    
                                                @endforelse
                                            </select> 
                                    </div>
                                    <div class="input-field">
                                        <input type="text" class="form-control" name="contact_name" id="contact_name" value="" required>
                                        <label for="contact_name">Contact name</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <input type="text" class="form-control" name="contact_email" id="contact_email" value="" required>
                                        <label for="contact_email">Contact Email</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <input type="text" class="form-control" name="contact_phone" id="contact_phone" value="" required>
                                        <label for="contact_phone">Contact phone</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <input type="text" class="form-control" name="description" id="description" value="" required>
                                        <label for="description">Description</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <label for="job_title">Job Title</label>
                                        <input required="required" id="job_title" name="job_title" type="text" value="">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <label for="job_type">Job Type</label>
                                        <br>
                                        <select id="job_type" class=" {{ $errors->has('job_type') ? ' is-invalid' : '' }}" name="job_type" value="{{ old('job_type') }}" required autofocus>
                                            <option selected value="Fulltime">Fulltime</option>
                                            <option value="Parttime">Part time</option>
                                            <option value="Temporary">Temporary</option>
                                            <option value="Freelance">Freelance</option>
                                            <option value="Seasonal">Seasonal</option>
                                        </select>
                                        <span class="help-block">Job Type</span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <label for="location">Location</label>
                                        <br>
                                        <input id="location" name="location" type="text" value="">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <label for="job_category">Job Category</label>
                                        <br>
                                        <select id="job_category" type="select" class="{{ $errors->has('job_category') ? ' is-invalid' : '' }}" name="job_category" value="{{ old('job_category') }}" required autofocus>
                                            <option selected value="Commercial">Commercial</option>
                                            <option value="Retail Sales">Retail sales</option>
                                            <option value="Creative">Creative</option>
                                            <option value="Technology">Technology</option>
                                            <option value="Marketing & PR">Marketing & PR</option>
                                            <option value="Fashion & Luxury">Fashion & luxury</option>
                                            <option value="Management & HR">Management & HR</option>
                                        </select>
                                        <span class="help-block">Job Category</span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <input id="salary" name="salary" type="text" value="">
                                        <label for="salary">Salary</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <input id="closing_at" name="closing_at" type="date" value="">
                                        <label for="closing_at">Closing Date</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <input type="checkbox" class="form-control" name="activated" id="activated" value="1">
                                        <label for="activated">Activated</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <input required="required" id="notes" name="notes" type="text" value="">
                                        <label for="notes">Notes</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-4">
                        <button type="submit" class="btn btn-block btn-lg gradient secondary waves-effect waves-light">
                            <span><strong>CREATE</strong></span>
                        </button>
                    </div>
                </form>
            </div>

            <!--Client-->
            <div id="createClient" class="col s12">
                <form method="POST" action="{{ action('ClientController@store') }}" accept-charset="UTF-8" id="candidateForm" role="form" data-parsley-validate="" enctype="multipart/form-data">
                @csrf
                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                    <section class="section-padding">
                        <div class="container">
                            <div class="row">
                                <h3 class="text-extrabold">Create a client</h3>
                                <div class="clearfix visible-sm"></div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <input type="text" class="form-control" name="society_name" id="society_name" value="" required>
                                        <label for="society_name">Society Name</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <input type="text" class="form-control" name="society_type" id="society_type" value="" required>
                                        <label for="society_type">Society Type</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <input type="text" class="form-control" name="contact_name" id="client_contact_name" value="" required>
                                        <label for="client_contact_name">Contact Name</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <input type="text" class="form-control" name="contact_job" id="client_contact_job" value="" required>
                                        <label for="client_contact_job">Contact Job</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <input required="required" id="client_contact_phone" name="contact_phone" type="phone" value="">
                                        <label for="client_contact_phone">Contact Phone</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <label for="client_contact_email">Contact Email</label>
                                        <br>
                                        <input required="required" id="client_contact_email" name="contact_email" type="email" value="">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12">
                                    <div class="input-field">
                                        <textarea class="materialize-textarea" id="notes" name="notes" cols="50" rows="10"></textarea>
                                        <label for="notes">Notes</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-4">
                        <button type="submit" class="btn btn-block btn-lg gradient secondary waves-effect waves-light">
                            <span><strong>CREATE</strong></span>
                        </button>
                    </div>
                </form>
            </div>

            <!--User-->
            <div id="createUser" class="col s12">
                <form method="POST" action="#!" accept-charset="UTF-8" id="candidateForm" role="form" data-parsley-validate="" enctype="multipart/form-data">
                @csrf
                <input name="_token" type="hidden" value="{{ csrf_token() }} ">
                    <section class="section-padding">
                        <div class="container">
                            <div class="row">
                                <h3 class="text-extrabold">Create a candidate account</h3>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <select id="gender" name="gender">
                                            <option value="">Choose an option</option>
                                            <option value="male">Male</option>
                                            <option value="female" selected="selected">Female</option>
                                        </select>
                                        <label for="gender" class="active">Gender</label>
                                    </div>
                                </div>
                                <div class="clearfix visible-sm"></div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <input type="text" class="form-control" name="first_name" id="user_first_name" value="" required>
                                        <label for="first_name">First name</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <input type="text" class="form-control" name="last_name" id="user_last_name" value="" required>
                                        <label for="last_name">Last name</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="input-field">
                                        <input required="required" id="current_location" name="current_location" type="text" value="">
                                        <label for="current_location">Current location</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <div class="input-field">
                                        <input id="user_address" name="address" type="text" value="">
                                        <label for="address">Address</label>
                                    </div>
                                </div>
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="input-field">
                                    <input type="checkbox" class="form-control" name="availability" id="availability" value="1">
                                    <label for="availability">Availability</label>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="file-field input-field"
                                            style="margin-top: 32px;">
                                        <div class="btn btn-lg primary waves-effect waves-light">
                                            <span><i class="material-icons">insert_photo</i> Photo</span>
                                            <input id="photo" size="20000000" accept=".pdf,.jpg,.doc,.docx,.png,.gif" name="photo" type="file">
                                        </div>
        
                                        <div class="existing-file">
                                            <a href="#!" target="_blank"><i class="material-icons">&#xE24D;</i> profil_picture.jpg</a>
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Upload your ID photo" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-8">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="input-field">
                                                <input id="country" name="country" type="text" value="">
                                                <label for="country">Country</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="input-field">
                                                <input id="nationality" name="nationality" type="text" value="">
                                                <label for="nationality">Nationality</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="input-field">
                                                <input class="datepicker" id="birth_date" name="birth_date" type="text" value="">
                                                <label for="birth_date">Birthdate</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="input-field">
                                                <input id="birth_place" name="birth_place" type="text" value="">
                                                <label for="birth_place">Birthplace</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-40">
                                <h3 class="text-extrabold">The professional profile</h3>
                                <div class="col-xs-12 col-sm-6">
        
                                    <div class="card card-panel passport">
                                        <div class="file-field input-field">
                                            <div class="btn btn-lg primary waves-effect waves-light">
                                                <span><i class="material-icons">&#xE24D;</i>Passport</span>
                                                <input id="passport" size="20000000" accept=".pdf,.jpg,.doc,.docx,.png,.gif" name="passport" type="file">
                                            </div>
                                            <div class="existing-file">
                                                <a href="#!" target="_blank"><i class="material-icons">&#xE24D;</i> passeport.jpg</a>
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" placeholder="Upload your passport" readonly>
                                            </div>
                                        </div>
        
                                        <div class="file-field input-field">
                                            <div class="btn btn-lg primary waves-effect waves-light">
                                                <span><i class="material-icons">&#xE24D;</i>CV</span>
                                                <input id="cv" size="20000000" accept=".pdf,.jpg,.doc,.docx,.png,.gif" name="cv" type="file">
                                            </div>
        
                                            <div class="existing-file">
                                                <a href="#!" target="_blank"><i class="material-icons">&#xE24D;</i>CV.pdf</a>
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" placeholder="Upload your Curriculum Vitae" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="input-field" style="margin-top: 5px;">
                                            <select id="user_job_sector" name="job_sector[]" placeholder="Type in or Select job sector you would be interested in.">
                                                <option value="">Choose an option</option>
                                                <option selected value="Commercial">Commercial</option>
                                                <option value="RetailSales">Retail sales</option>
                                                <option value="Creative">Creative</option>
                                                <option value="Technology">Technology</option>
                                                <option value="Marketing & PR">Marketing & PR</option>
                                                <option value="Fashion & Luxury">Fashion & luxury</option>
                                                <option value="Management & HR">Management & HR</option>
                                            </select>
                                            <label for="job_sector" class="active">Interest in job sector</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="input-field">
                                            <select id="experience" required="required" name="experience">
                                                <option value="">Choose an option</option>
                                                <option value="3m">0 - 6 month</option>
                                                <option value="6m">6 month - 1 year</option>
                                                <option value="1y">1 - 2 years</option>
                                                <option value="2y">2+ years</option>
                                                <option value="5y" selected="selected">5+ years</option>
                                                <option value="10y">10+ years</option>
                                            </select>
                                            <label for="experience" class="active">Experience</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12">
                                    <div class="input-field">
                                        <textarea class="materialize-textarea" id="user_description" name="description" cols="50" rows="10"></textarea>
                                        <label for="description">Short description for your profile, as well as more personnal informations (e.g. your hobbies/interests ). You can also paste any link you want.</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12">
                                    <div class="input-field">
                                        <textarea class="materialize-textarea" id="notes" name="notes" cols="50" rows="10"></textarea>
                                        <label for="notes">Notes</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-4">
                        <button type="submit" class="btn btn-block btn-lg gradient secondary waves-effect waves-light">
                            <span><strong>CREATE</strong></span>
                        </button>
                    </div>
                </form>
            </div>
    </section>
    <script>
        var ctx = document.getElementById('myChart').getContext('2d');
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [{
                    label: "My First dataset",
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: [0, 10, 5, 2, 20, 30, 45],
                }]
            },

            // Configuration options go here
            options: {}
        });

    </script>
    <script type="text/javascript">
        $(function () {
            $('select').material_select();
        });
        onAppReady(function() {
            
            $('.preload').remove();
        });
    </script>
</body>
</html>