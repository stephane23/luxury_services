@extends('layouts.layout')

@section('content')
    <!-- Page Content-->
    <section class="single-project-section section-padding light-gray-bg">
        <div class="container">
            <div class="project-overview">
                <div class="row mb-80">
                    <div class="col-xs-12 col-md-8">
                        <p class="ref grey-text no-margin">Ref. {{ $jobOffer->id }}</p>
                        <h2>{{ $jobOffer->job_title }}</h2>
                        <p>{{ $jobOffer->description }}</p>
                    </div>

                    <div class="col-xs-12 col-md-4 quick-overview">
                        <ul class="portfolio-meta">
                            <li><span> Pulished at </span>{{ $jobOffer->created_at }}</li>
                            <li><span> Position </span>{{ $jobOffer->job_title }}</li>
                            <li><span> Contract Type </span>{{ $jobOffer->job_type }}</li>
                            <li><span> Salary </span>{{ $jobOffer->salary }} &euro;</li>
                            <li><span> Location </span>{{ $jobOffer->location }}</li>
                            <li><span> Closing date </span>{{ $jobOffer->closing_at }}</li>
                        </ul>
                        
                        @if(session('error'))
                            <div class="btn btn-block btn-failed mt-30 waves-effect waves-light disabled">{{ session('error') }}</div>
                        @elseif(session('confirmation'))
                            <div class="btn btn-block btn-success mt-30 waves-effect waves-light disabled">{{ session('confirmation') }}</div>
                        @endif
                        
                        @if (isset($totalRounded) && $totalRounded == 100 && $jobOffer !== 0) 
                            <a class="btn btn-block gradient primary mt-30 waves-effect waves-light" href="{{ url("jobapplication/".$jobOffer->id )}}">Apply for this job</a>
                        @else
                            <a class="btn btn-block gradient primary mt-30 waves-effect waves-light" href="{{ route('profile.show') }}">Apply for this job</a>
                        @endif
                        
                    </div>
                </div>
            </div>

            <nav class="single-post-navigation no-margin" role="navigation">
                <div class="row">

                    <div class="col-xs-6 col-sm-6 col-md-4">
                        <div class="previous-post-link">
                            <a class="btn border primary waves-effect waves-dark" href="{{ URL::to( '/joboffer/' . $previous ) }}">
                                <i class="fa fa-long-arrow-left"></i>Previous
                            </a>
                        </div>
                    </div>
                    <div class="hidden-xs hidden-sm col-md-4"></div>


                    <div class="col-xs-6 col-sm-6 col-md-4">
                        <div class="next-post-link">
                            <a class="btn border primary waves-effect waves-dark" href="{{ URL::to( '/joboffer/' . $next ) }}">
                                Next <i class="fa fa-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>

                </div>
            </nav>
        </div>
    </section>
@endsection




