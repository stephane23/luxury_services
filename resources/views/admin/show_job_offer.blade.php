@extends('layouts.layout-admin')

@section('content')
    <!-- Page Content-->
    <section class="single-project-section section-padding light-gray-bg">
        <div class="container">
            <div class="project-overview">
                <div class="row mb-80">
                    <div class="col-xs-12 col-md-8">
                        <p class="ref grey-text no-margin">Ref. {{ $jobOffer->id }}</p>
                        <h2>{{ $jobOffer->job_title }}</h2>
                        <p>{{ $jobOffer->description }}</p>
                    </div>

                    <div class="col-xs-12 col-md-4 quick-overview">
                        <ul class="portfolio-meta">
                            <li><span> Pulished at </span>{{ $jobOffer->created_at }}</li>
                            <li><span> Position </span>{{ $jobOffer->job_title }}</li>
                            <li><span> Contract Type </span>{{ $jobOffer->job_type }}</li>
                            <li><span> Salary </span>{{ $jobOffer->salary }} &euro;</li>
                            <li><span> Location </span>{{ $jobOffer->location }}</li>
                            <li><span> Closing date </span>{{ $jobOffer->closing_at }}</li>
                        </ul>
                       <form type="hidden" method="post" action="{{ action('AdminController@destroyJobOffer', ['id' => $jobOffer->id]) }}">
                            @csrf
                            <input name="_token" type="hidden" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-block btn-failed primary mt-30 waves-effect waves-dark" name="_method" value="DELETE">Delete offer</a>
                        </form>
                    </div>
                </div>
            </div>

            <div class="portfolio portfolio-with-title col-2 gutter mt-30">
                    @forelse($jobApplications as $jobApplication)
                        <div class="portfolio-wrapper">
                            <div class="card job-card">
                                <div class="card-content">
                                    <span class="title">
                                        <span class="card-title">Name : {{ $jobApplication->user->last_name }} {{ $jobApplication->user->first_name }}</span>
                                        <span class="ref grey-text">Ref. {{ $jobApplication->id }}</span>
                                    </span>
                                    
                                    <p class="truncate-text"></p>
                                </div>
                                <div class="card-action">
                                    
                                    <form type="hidden" method="post" action="{{ action('AdminController@destroyJobApplication', ['id' => $jobApplication->id]) }}">
                                    @csrf
                                    <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                    <a class="btn btn-md gradient primary mt-30 waves-effect waves-light" href="{{ route('admin.profile', ['id' => $user->id]) }}">View profile</a>
                                    <button class="btn btn-md primary btn-failed mt-30 waves-effect waves-dark" name="_method" value="DELETE">Delete job application</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @empty
                    @endforelse    
                </div>

            <nav class="single-post-navigation no-margin" role="navigation">
                <div class="row">

                    <div class="col-xs-6 col-sm-6 col-md-4">
                        <div class="previous-post-link">
                            <a class="btn border primary waves-effect waves-dark" href="{{ URL::to( '/admin/joboffer/' . $previous ) }}">
                                <i class="fa fa-long-arrow-left"></i>Previous
                            </a>
                        </div>
                    </div>
                    <div class="hidden-xs hidden-sm col-md-4"></div>


                    <div class="col-xs-6 col-sm-6 col-md-4">
                        <div class="next-post-link">
                            <a class="btn border primary waves-effect waves-dark" href="{{ URL::to( '/admin/joboffer/' . $next ) }}">
                                Next <i class="fa fa-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>

                </div>
            </nav>
        </div>
    </section>
@endsection




