@extends('layouts.layout-admin')

    @section('content')
        <!-- Page Content-->
        <section id="job-offers" class="section-padding gray-bg">
            <div class="container">
                <div class="text-center mb-40">
                    <h2 class="section-title">We have the perfect job for you</h2>
                    <p class="section-sub">just like our candidates, we offer quality, serious & reliable job offers.</p>
                </div>
                <div class="portfolio-container">
                    <ul class="portfolio-filter brand-filter text-center">
                        <li class="active waves-effect waves-light" data-group="all">All</li>
                        <li class=" waves-effect waves-light" data-group="Commercial">Commercial</li>
                        <li class=" waves-effect waves-light" data-group="Retail Sales">Retail Sales</li>
                        <li class=" waves-effect waves-light" data-group="Creative">Creative</li>
                        <li class=" waves-effect waves-light" data-group="Technology">Technology</li>
                        <li class=" waves-effect waves-light" data-group="Marketing & PR">Marketing & PR</li>
                        <li class=" waves-effect waves-light" data-group="Fashion & Luxury">Fashion & Luxury</li>
                        <li class=" waves-effect waves-light" data-group="Management & HR">Management & HR</li>
                    </ul>

                    <div class="portfolio portfolio-with-title col-2 gutter mt-30">
                    @forelse($jobsList as $jobs)
                        <div class="portfolio-item" data-groups='["all", "{{ $jobs->job_category }}"]'>
                            <div class="portfolio-wrapper">
                                <div class="card job-card">
                                    <div class="card-content">
                                        <span class="title">
                                            <span class="card-title">{{ $jobs->job_title }}</span>
                                            <span class="ref grey-text">Ref. {{ $jobs->id }}</span>
                                        </span>
                                        <div class="metas mb-20">
                                            <div class="meta">
                                                <i class="material-icons">&#xE53E;</i>{{ $jobs->salary }} &euro;
                                            </div>
                                            <div class="meta">
                                                <i class="material-icons">&#xE916;</i>{{ $jobs->closing_at }}
                                            </div>
                                            <div class="meta">
                                                <i class="material-icons">&#xE55F;</i>{{ $jobs->location }}
                                            </div>
                                        </div>
                                        <p class="truncate-text">{{ $jobs->description }}</p>
                                    </div>
                                    <div class="card-action">
                                        <form type="hidden" method="post" action="{{ action('AdminController@destroyJobOffer', ['id' => $jobs->id]) }}">
                                        @csrf
                                        <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                        <a class="btn btn-md primary border waves-effect waves-dark" href="{{ route('admin.showJobOffer', ['id' => $jobs->id]) }}">Details</a>
                                        <button type="submit" class="btn btn-md primary border btn-failed waves-effect waves-dark" name="_method" value="DELETE">Delete</a>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <p>No job offers available</p>
                    @endforelse
                        <div class="no-item"></div>
                    </div>
                </div>
            </div>
        </section>

        <section class="cta-candidate bg-fixed bg-cover overlay dark-5 padding-top-70 padding-bottom-50">
            <div class="container">
                <div class="row">
                    <div class="valign-wrapper text-center">
                        <div class="hero-intro valign-cell">
                            <h2 class="tt-headline clip is-full-width no-margin">
                                <span>You are </span>
                                <span class="tt-words-wrapper">
                                    <b class="is-visible">Commercial</b>
                                    <b>Creative</b>
                                    <b>Marketing & PR</b>
                                    <b>Technology</b>
                                    <b>Fashion & luxury</b>
                                    <b>Retail sales</b>
                                </span>
                            </h2>
                            <h3 class="c-secondary mb-30 no-margin">Sign-up and apply for jobs now</h3>
                            <p class="white-text section-sub">Each one of your skills is seen as precious resource to us, each one of your personality traits are considered as an added on value. Joining us is allowing our recruitment expertise and personal guidance to bring the best out in you by choosing the most suited position.</p>

                            <a href="{{ route('register') }}" class="btn border secondary waves-effect waves-light mt-40">Join us</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
