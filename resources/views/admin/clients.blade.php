@extends('layouts.layout-admin')

    @section('content')
        <!-- Page Content-->
        <section id="clients" class="section-padding gray-bg">
            <div class="container">
                <div class="portfolio portfolio-with-title col-2 gutter mt-30">
                    @forelse($clientList as $clients)
                        <div class="portfolio-wrapper">
                            <div class="card job-card">
                                <div class="card-content">
                                    <span class="title">
                                        <span class="card-title">Society Name : {{ $clients->society_name }}</span>
                                        <span class="ref grey-text">Ref. {{ $clients->id }}</span>
                                    </span>
                                    <ul class="portfolio-meta">
                                        <li><span>Society Type :</span> {{ $clients->society_type }}</li>
                                        <li><span>Contact Name :</span> {{ $clients->contact_name}}</li>
                                        <li><span>Contact Job :</span> {{ $clients->contact_job }}</li>
                                        <li><span>Contact Phone :</span> {{ $clients->contact_phone }}</li>
                                        <li><span>Contact Email :</span> {{ $clients->contact_email }}</li>
                                        <li><span>Notes :</span> {{ $clients->notes }}</li>
                                        <li><span>Created At :</span> {{ $clients->created_at }}</li>
                                        <li><span>Updated At :</span> {{ $clients->updated_at }}</li>
                                        <li><span>Deleted At :</span> {{ $clients->deleted_at }}</li>
                                    </ul>
                                    <p class="truncate-text"></p>
                                </div>
                                <div class="card-action">
                                    <form type="hidden" method="post" action="{{ action('AdminController@destroyClient', ['id' => $clients->id]) }}">
                                    @csrf
                                    <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-md primary border btn-failed waves-effect waves-dark" name="_method" value="DELETE">Delete</a>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @empty
                    @endforelse    
                </div>
            </div>
        </section>
                    
    @endsection
