@extends('layouts.layout')

    @if(session('error'))
        <div class="btn btn-block btn-failed mt-30 waves-effect waves-light disabled">{{ session('error') }}</div>
    @elseif(session('confirmation'))
        <div class="btn btn-block btn-success mt-30 waves-effect waves-light disabled">{{ session('confirmation') }}</div>
    @endif

    @section('content')

    @include('partials/slider')
    
        <!-- Page Content-->
        <!--<section class="full-width promo-box brand-bg ptb-50">-->
            <!--<div class="container">-->
                <!---->
            <!--</div>-->
        <!--</section>-->
        <section class="ptb-50 brand-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 mb-40">
                        <div class="promo-info">
                            <span class="white-text">Whether you're an employer or a candidate looking for jobs</span>
                            <h3 class="white-text text-bold text-uppercase no-margin">WE HAVE THE SOLUTION FOR YOU</h3>
                        </div>
                        <div class="promo-btn">
                            <a href="{{ route('contact') }}" class="btn border secondary waves-effect waves-light">Contact Us</a>
                        </div>
                    </div>
                </div>

                <hr class="mt-10 mb-50">

                <div class="text-center">
                    <h2 class="section-title primary-text">Who We are</h2>
                    <p class="section-sub white-text">Luxury Services is a leading professional recruitment consultancy specialising in the recruitment of permanent, contract and temporary positions on behalf of the world’s top employers.</p>
                </div>
                <br>

                <div class="row">
                    <div class="col-sm-6">
                        <div id="project-slider" class="carousel slide boot-slider" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#project-slider" data-slide-to="0" class="active"></li>
                                <li data-target="#project-slider" data-slide-to="1"></li>
                            </ol>

                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <img class="img-responsive" src="assets/img/slide1.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img class="img-responsive" src="assets/img/slide2.jpg" alt="">
                                </div>
                            </div>

                            <a class="left carousel-control" href="#project-slider" role="button" data-slide="prev">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#project-slider" role="button" data-slide="next">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <h3 class="primary-text">Our philosophy</h3>
                        <p class="white-text">Our role is to work with employers and job seekers to facilitate a successful match. This can range from advising a global company on a candidate sourcing strategy to helping a job seeker find their dream job.</p>
                    </div>

                    <div class="text-center">
                        <a href="{{ route('company') }}" class="btn gradient secondary waves-effect waves-light mt-40">More about us</a>
                    </div>
                </div>
            </div>
        </section>

        <section class="cta-candidate bg-fixed bg-cover overlay dark-5 padding-top-70 padding-bottom-50">
            <div class="container">
                <div class="row">
                    <div class="valign-wrapper text-center">
                        <div class="hero-intro valign-cell">
                            <h2 class="tt-headline clip is-full-width no-margin">
                                <span>You are </span>
                                <span class="tt-words-wrapper">
                                    <b class="is-visible">Commercial</b>
                                    <b>Creative</b>
                                    <b>Marketing & PR</b>
                                    <b>Technology</b>
                                    <b>Fashion & luxury</b>
                                    <b>Retail sales</b>
                                </span>
                            </h2>
                            <h3 class="c-secondary mb-30 no-margin">Sign-up and apply for jobs now</h3>
                            <p class="white-text section-sub">Each one of your skills is seen as precious resource to us, each one of your personality traits are considered as an added on value. Joining us is allowing our recruitment expertise and personal guidance to bring the best out in you by choosing the most suited position.</p>

                            <a href="{{ route('register') }}" class="btn border secondary waves-effect waves-light mt-40">Join us</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="job-offers" class="section-padding gray-bg">
            <div class="container">
                <div class="text-center mb-40">
                    <h2 class="section-title">We have the perfect job for you</h2>
                    <p class="section-sub">just like our candidates, we offer quality, serious & reliable job offers.</p>
                </div>
                <div class="portfolio-container">
                    <ul class="portfolio-filter brand-filter text-center">
                        <li class="active waves-effect waves-light" data-group="all">All</li>
                        <li class=" waves-effect waves-light" data-group="Commercial">Commercial</li>
                        <li class=" waves-effect waves-light" data-group="Retail Sales">Retail Sales</li>
                        <li class=" waves-effect waves-light" data-group="Creative">Creative</li>
                        <li class=" waves-effect waves-light" data-group="Technology">Technology</li>
                        <li class=" waves-effect waves-light" data-group="Marketing & PR">Marketing & PR</li>
                        <li class=" waves-effect waves-light" data-group="Fashion & Luxury">Fashion & Luxury</li>
                        <li class=" waves-effect waves-light" data-group="Management & HR">Management & HR</li>
                    </ul>
                    @if(session('error'))
                        <div class="btn btn-block btn-failed mt-30 waves-effect waves-light disabled">{{ session('error') }}</div>
                    @elseif(session('confirmation'))
                        <div class="btn btn-block btn-success mt-30 waves-effect waves-light disabled">{{ session('confirmation') }}</div>
                    @endif
                    <div class="portfolio portfolio-with-title col-2 gutter mt-30">
                    @forelse($jobsList as $jobs)
                        <div class="portfolio-item" data-groups='["all", "{{ $jobs->job_category }}"]'>
                            <div class="portfolio-wrapper">
                                <div class="card job-card">
                                    <div class="card-content">
                                        <span class="title">
                                            <span class="card-title">{{ $jobs->job_title }}</span>
                                            <span class="ref grey-text">Ref. {{ $jobs->id }}</span>
                                        </span>
                                        <div class="metas mb-20">
                                            <div class="meta">
                                                <i class="material-icons">&#xE53E;</i>{{ $jobs->salary }} &euro;
                                            </div>
                                            <div class="meta">
                                                <i class="material-icons">&#xE916;</i>{{ $jobs->closing_at }}
                                            </div>
                                            <div class="meta">
                                                <i class="material-icons">&#xE55F;</i>{{ $jobs->location }}
                                            </div>
                                        </div>
                                        <p class="truncate-text">{{ $jobs->description }}</p>
                                    </div>
                                    <div class="card-action">
                                        <a class="btn btn-md primary border waves-effect waves-dark" href="{{ route('joboffer.show', ['id' => $jobs->id]) }}">Details</a>
                                        @if (isset($totalRounded) && $totalRounded == 100)
                                            <a class="btn btn-md primary waves-effect waves-light" href="{{ url("jobapplication/".$jobs->id )}}">Apply</a>
                                        @else
                                            <a class="btn btn-md primary waves-effect waves-light" href="{{ route('profile.show') }}">Apply</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <p>No job offers available</p>
                    @endforelse
                        <div class="no-item"></div>
                    </div>
                    <div class="text-center">
                        <a href="{{ route('joboffer') }}" class="btn gradient secondary waves-effect waves-light mt-30">View all job offers</a>
                    </div>
                </div>
            </div>
        </section>
    @endsection

