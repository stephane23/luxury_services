<!-- Navigation -->
<header id="header" class="tt-nav transparent-header">
    <div class="header-sticky semi-transparent">
        <div class="container">
            <div id="materialize-menu" class="menuzord">
                <a class="brand logo-brand" href="/"></a>
                <div id="mobile-menu">
                    <div class="nav-wrapper" id="toggle">
                        <span class="top"></span>
                        <span class="middle"></span>
                        <span class="bottom"></span>
                    </div>
                    <div class="icon-wrapper">
                        <span class="account">
							<a href="{{ route('profile.show') }}">
                                <i class="material-icons">&#xE7FD;</i>
                            </a>
						</span>
                    </div>
                    <div class="nav-overlay" id="overlay">
                        <nav class="overlay-menu">
                            <ul>
                                <li><a href="{{ route('homepage') }}">Home</a></li>
                                <li><a href="{{ route('joboffer') }}">Jobs Offers</a></li>
                                <li><a href="{{ route('company') }}">About Us</a></li>
                                <li><a href="{{ route('contact') }}">Contact</a></li>
                                <li>-</li>
                                <li><a href="{{ route('profile.show') }}">Profile</a></li>
                                <li><a href="{{ route('logout') }}">Logout</a></li>
                                @if(Auth::user(['admin']))
                                <li><a href="{{ route('admin') }}">Admin</a></li>
                                @endif
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="menuzord-menu pull-right light" role="navigation">
                        @if (Auth::guest())
                        <li class=" dropdown">
                            <a target="_self" href="{{ route('login')}}">
                                <i class=""></i>
                                <span>Login</span>
                            </a>
                        </li>
                        <li class=" dropdown">
                            <a target="_self" href="{{ route('register') }}" class="btn bn-lg gradient secondary btn-block waves-effect waves-light btn-register">
                                <i class=""></i>
                                <span>Sign up</span>
                            </a>
                        </li>
                        @else
                         <li class=" dropdown">
                            <a target="_self" href="{{ route('logout')}}">
                                <i class=""></i>
                                <span>Log out</span>
                            </a>
                        </li>
                        <li class=" dropdown">
                            <a target="_self" href="{{ route('profile.show') }}">
                                <i class=""></i>
                                <span>Profile</span>
                            </a>
                        </li>
                        @endif
                        @if(Auth::user(['admin']))
                        <li>
                            <a target="_self" href="{{ route('admin') }}">
                                <i class=""></i>
                                <span>Admin</span>
                            </a>
                        </li>
                        @endif
                    </ul>
                    <ul class="menuzord-menu pull-right light" role="navigation">
                        <li class="active dropdown">
                            <a target="_self" href="{{ route('homepage') }}">
                                <i class=""></i>
                                <span>Home</span>
                            </a>
                        </li>
                        <li class=" dropdown">
                            <a target="_self" href="{{ route('joboffer') }}">
                                <i class=""></i>
                                <span>Jobs offers</span>
                            </a>
                        </li>
                        <li class=" dropdown">
                            <a target="_self" href="{{ route('company') }}">
                                <i class=""></i>
                                <span>About us</span>
                            </a>
                        </li>
                        <li class=" dropdown">
                            <a target="_self" href="{{ route('contact') }}">
                                <i class=""></i>
                                <span>Contact</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>