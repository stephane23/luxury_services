<!-- Navigation -->
<header id="header" class="tt-nav transparent-header">
    <div class="header-sticky semi-transparent">
        <div class="container">

            <div id="materialize-menu" class="menuzord">

                <a class="brand logo-brand" href="/"></a>
                <div id="mobile-menu">
                    <div class="nav-wrapper" id="toggle">
                        <span class="top"></span>
                        <span class="middle"></span>
                        <span class="bottom"></span>
                    </div>
                    <div class="icon-wrapper">
                        <span class="account">
							<a href="{{ route('profile.show') }}">
                                <i class="material-icons">&#xE7FD;</i>
                            </a>
						</span>
                    </div>
                    <div class="nav-overlay" id="overlay">
                        <nav class="overlay-menu">
                            <ul>
                                <li><a href="{{ route('homepage') }}">Home</a></li>
                                <li><a href="{{ route('admin') }}">Dashboard</a></li>
                                <li><a href="{{ route('admin.indexJobOffers') }}">Jobs Offers</a></li>
                                <li><a href="{{ route('admin.clients') }}">Clients</a></li>
                                <li>-</li>
                                <li><a href="{{ route('logout') }}">Logout</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="menuzord-menu pull-right light" role="navigation">
                         <li class=" dropdown">
                            <a target="_self" href="{{ route('logout')}}">
                                <i class=""></i>
                                <span>Log out</span>
                            </a>
                        </li>
                    </ul>
                    <ul class="menuzord-menu pull-right light" role="navigation">
                        <li class="active dropdown">
                            <a target="_self" href="{{ route('homepage') }}">
                                <i class=""></i>
                                <span>Home</span>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a target="_self" href="{{ route('admin') }}">
                                <i class=""></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li class=" dropdown">
                            <a target="_self" href="{{ route('admin.indexJobOffers') }}">
                                <i class=""></i>
                                <span>Jobs offers</span>
                            </a>
                        </li>
                        <li class=" dropdown">
                            <a target="_self" href="{{ route('admin.clients') }}">
                                <i class=""></i>
                                <span>Clients</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>

