<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsTable extends Migration {

	public function up()
	{
		Schema::create('clients', function(Blueprint $table) {
			$table->increments('id');
			$table->text('society_name');
			$table->text('society_type');
			$table->text('contact_name');
			$table->text('contact_job');
			$table->string('contact_phone', 15);
			$table->string('contact_email', 50);
			$table->text('notes');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('clients');
	}
}