<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->enum('gender', array('Male', 'Female'));
			$table->string('first_name', 50);
			$table->string('last_name', 50);
			$table->text('address');
			$table->text('country');
			$table->string('nationality', 50);
			$table->boolean('passport');
			$table->text('curriculum_vitae');
			$table->text('profil_picture');
			$table->text('current_location');
			$table->date('birth_date');
			$table->text('birth_place');
			$table->text('email');
			$table->string('password', 100);
			$table->boolean('availability');
			$table->enum('job_sector', array('Commercial', 'Retailsales', 'Creative', 'Technology', 'Marketing&PR', 'Fashion&luxury', 'Management&HR'));
			$table->enum('experience', array('0-6months', '6months-1year', '1-2years', '2+years', '5+years', '10+years'));
			$table->text('short_description');
			$table->text('files');
			$table->tinyInteger('admin')->default('0');
			$table->text('notes');
			$table->rememberToken();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('users');
	}
}