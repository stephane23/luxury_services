<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('homepage');
// });

Route::get('/profile', 'ProfileController@show')->name('profile.show');
Route::get('/profile/destroy', 'ProfileController@destroy')->name('profile.delete');
Route::post('/profile', 'ProfileController@store')->name('profile.store');

Auth::routes();

Route::get('/', 'HomeController@index')->name('homepage');
Route::get('/joboffers', 'JobOfferController@index')->name('joboffer');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::post('/contact', 'HomeController@sendMail')->name('sendMail');
Route::get('/about-us', 'HomeController@company')->name('company');
Route::get('joboffer/details', 'JobOfferController@show')->name('details');
Route::get('/jobapplication/{id}', 'JobApplicationController@store');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('../storage/app/images/{path}', 'MediaController@show')->name('media');


Route::middleware('admin')->prefix('/admin')->group(function () {
    Route::get('/', 'AdminController@show')->name('admin');
    Route::get('/joboffers', 'AdminController@indexJobOffers')->name('admin.indexJobOffers');
    Route::get('/joboffer/{id}', 'AdminController@showJobOffer')->name('admin.showJobOffer');
    Route::delete('/joboffer/{id}', 'AdminController@destroyJobOffer')->name('admin.destroyJobOffer');
    Route::get('/clients', 'AdminController@showClients')->name('admin.clients');
    Route::delete('/clients/{id}', 'AdminController@destroyClient')->name('admin.destroyClient');
    Route::delete('/jobapplications{id}', 'AdminController@destroyJobApplication')->name('admin.destroyJobApplication');
    Route::get('/profile', 'AdminController@showProfile')->name('admin.profile');
});

Route::resource('client', 'ClientController');
Route::resource('joboffer', 'JobOfferController');
